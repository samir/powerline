if [[ "$TERM" != "linux" ]]; then
    command -v powerline-daemon &>/dev/null
    if [ $? -eq 0 ]; then
        powerline-daemon -q
        POWERLINE_BASH_CONTINUATION=1
        POWERLINE_BASH_SELECT=1
        . /usr/share/powerline/bindings/bash/powerline.sh
    fi
fi

